# this is for setting up on Amazon Linux ec2 instance

##Mongo
http://docs.mongodb.org/ecosystem/platforms/amazon-ec2/
sudo yum -y update
sudo yum install -y mongodb-org-server mongodb-org-shell mongodb-org-tools
sudo service mongod start

##Node NPM
git clone git://github.com/joyent/node.git
cd node
git checkout v0.10.33    #match up with desktop
./configure
make
sudo make install

sudo su
vi /etc/sudoers
add /usr/local/bin to the path so sudo can find node

git clone https://github.com/isaacs/npm.git
cd npm
sudo make install

sudo npm install bower -g