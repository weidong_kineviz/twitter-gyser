'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TopicSchema = new Schema({
  topic: String,
  keywords: Array,
  active: Boolean
});

module.exports = mongoose.model('Topic', TopicSchema);

// var Tweet = mongoose.model('Tweet', TweetSchema);
// Tweet.count({}, function(err, cnt){console.log(cnt)})
// Tweet.find({}).limit(10).exec(function(err,tw){console.log(tw);})