'use strict';

var express = require('express');
var controller = require('./tweet.controller');

var router = express.Router();

// http://api/tweets/...
router.get('/', controller.index);
router.get('/getTweets/:topic', controller.getTweets);
router.get('/getTweets/:topic/:startTime', controller.getTweets);
router.post('/getTweets/:topic', controller.startTweets);
router.put('/getTweets/:topic', controller.stopTweets);
router.delete('/getTweets/:topic', controller.destroyTweets);

router.get('/getTopics', controller.getTopics);
router.get('/resetTopics', controller.resetTopics);
router.get('/firehoseStatus', controller.firehoseStatus);

module.exports = router;