'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TweetSchema = new Schema({
  id: Number,
  raw: Object,
  created_at: Date,
  photo: String,
  description: String,
  text_keywords: String,
  followers_count: Number,
  in_reply_to_status_id: Number,
  in_reply_to_status_id_str: String,
  in_reply_to_user_id: Number,
  in_reply_to_user_id_str: String,
  in_reply_to_screen_name: String,
  screenName: String,
  keyword: String,
  topic: Array,
  latitude: Number,
  longitude: Number,
  isBlacklisted: Boolean,
  location: String
});

module.exports = mongoose.model('Tweet', TweetSchema);

// var Tweet = mongoose.model('Tweet', TweetSchema);
// Tweet.count({}, function(err, cnt){console.log(cnt)})
// Tweet.find({}).limit(10).exec(function(err,tw){console.log(tw);})
// db.tweets.update({keyword: "ebola"}, {$set:{topic:["Rackspace"]}}, {multi:true}