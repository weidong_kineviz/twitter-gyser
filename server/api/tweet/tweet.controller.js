/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /tweets              ->  index
 * POST    /tweets              ->  create
 * GET     /tweets/:id          ->  show
 * PUT     /tweets/:id          ->  update
 * DELETE  /tweets/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var twitter = require('./../../components/twitter/twitter.js');
var Tweet = require('./tweet.model');
var Topic = require('./topic.model');
var $ = require('jquery');
var bFirehoseRunning = false;

function autoRun(){
  // twitter.reInitTopics(); //seed if empty
  // twitter.streemOnTopics();

  setInterval(twitter.collectTopics, 10000);
}

autoRun();

exports.getTopics = function(req, res){
  console.log("client asks for topics");
  // console.log("returning:");
  // console.log(twitter.topics);
  return res.json(200, twitter.topics);
  // Topic.find({}, function(err, topics){
  //   var items = _.reduce(topics, function(sum, topic){
  //     sum.push(topic.topic);
  //     return sum;
  //   }, []);
  //   return res.json(200, items);
  // });
}

exports.resetTopics = function(req, res){
  console.log("resetting firehose tracking topics");
  twitter.resetTopics();
};

// gets list of tweets matching topics passed in
exports.getTweets = function(req, res){
  console.log("received getTweets request");
  // console.log(req.params);
  console.log("    clent ask for:" + req.params.topic);
  console.log("    starting:" + req.params.startTime);
  var reqTopics = req.params.topic.split(',');
  // console.log(reqTopics);
  var startTime = req.params.startTime ? new Date(req.params.startTime) : new Date("2000-01-01");
  Tweet.find({topic: {$in: reqTopics}, created_at:{'$gt': startTime}})
    .limit(10)
    .exec(function(err, tweets) {
      console.log('        num tweets found '+tweets.length);
      return res.json(200, tweets);
    });
  // Tweet.find({keyword: req.params.topic}, function(err, tweets) {
  //   console.log('num tweets found '+tweets.length);
  //   return res.json(200, tweets);
  // });
}

// stops twitter stream
exports.stopTweets = function(req, res) {
  twitter.stopTweets(req.params.topic, function(err, data){
    bFirehoseRunning = false;
    return res.send(200, data);
  });
}

exports.firehoseStatus=function(req, res){
  return res.send(200, bFirehoseRunning);
}

// starts twitter stream
exports.startTweets = function(req, res){
  twitter.streemOnTopics(req.params.topic, function(err, data){
    bFirehoseRunning = true;
    return res.json(200, data);
  });
}

// destroys tweets matching topic in DB
exports.destroyTweets = function(req, res){
  Tweet.find({keyword: req.params.topic}, function(err, tweets){
    for (var i = 0; i < tweets.length; i++) {
      tweets[i].remove();
    }
    return res.send(200);
  })
}

// Get list of all tweets
exports.index = function(req, res) {
  Tweet.find()
  .limit(500)
  .exec(function(err, tweets) {
    if(err) {
      console.log("***** Tweet  find error " );
      return handleError(res, err); 
    }
    console.log('*******indexing, this should not happen,  num tweets found '+tweets.length);
    return res.json(200, tweets);
  });

  // Tweet.count({}, function (err, cnts) {
  //   if(err) { return handleError(res, err); }
  //   console.log(cnts);
  //   return res.json(200, cnts);
  // });
  // Tweet.find(function (err, tweets) {
  //   if(err) { return handleError(res, err); }
  //   return res.json(200, tweets);
  // });
};

function handleError(res, err) {
  console.log(err);
  return res.send(500, err);
}