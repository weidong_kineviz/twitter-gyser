"use strict";

var Twit = require('twit');
var Tweet = require('./../../api/tweet/tweet.model.js');
var Topic = require('./../../api/tweet/topic.model.js');
var _ = require('lodash');
var $ = require('jquery');
var filter = require('wordfilter');
var extractor = require('keyword-extractor');

var secrets = {
  consumerKey: process.env['TWITTER_CONSUMER_KEY'],
  consumerSecret: process.env['TWITTER_CONSUMER_SECRET'],
  accessToken: process.env['TWITTER_ACCESS_TOKEN'],
  accessTokenSecret: process.env['TWITTER_ACCESS_TOKEN_SECRET']
};

var T = new Twit({
  consumer_key: secrets.consumerKey,
  consumer_secret: secrets.consumerSecret,
  access_token: secrets.accessToken,
  access_token_secret: secrets.accessTokenSecret
});

var stream;

var _topics=[];
var defaultTopics=[
  {
    topic: "Rackspace",
    keywords:['@airbrake', '@mail_gun', 
    '@objectrocket', '@rackspace', '@GeekdomSF', '@redistogo', 
    'mongodb', 'redis', 'elasticsearch', '#spark' ],
    active: true
  },
  {
    topic: "Ebola",
    keywords: ["ebola"],
    active: true
  },
  {
    topic: "Box",
    keywords: ["box", "@BoxHQ", "@BoxPlatform"],
    active: true
  },
  {
    topic: "WebGL",
    keywords: ["webGL", "threejs", "@mrdoob"],
    active: true
  },
  {
    topic: "Mobile",
    keywords: ["$AAPL", "$NOK", "$GOOG", "$MSFT", "$VZ", "$AMZN", "$BBRY", "$RIMM", 
      "$TMUS", "$S", "$T", "@asymco", "@verge", "@daringfireball", "@evleaks", 
      "@crackberrykevin", "@appleleak", "@appletweets", "@benedictevans", "@benbajarin"],
    active: true
  }
];

function stopTweets(topic, callback) {
  stream.stop();
  callback();
}

function streemOnTopics(topic, callback){ //we are not using this topic that passed in
  var that = this;
  that.callback = callback;
  // topics.

  Topic.find({}, function(err, topics){
    var filterKeywords = _.reduce(topics, function(sum, topic){
      if(topic.active){

        return sum.concat(topic.keywords);
      }
      else
        return sum;
    }, []);
    // console.log(filterKeywords);

    streamTweets(filterKeywords, that.callback);
  })
}

function streamTweets(filterKeywords, callback) {
  // console.log('started', topic);
  
  var globe = ['-180', '-90', '180', '90'];
  console.log("Tracking: "+filterKeywords);
  stream = T.stream('statuses/filter', { track: filterKeywords}); 
  // stream = T.stream('statuses/filter', { track: ['@airbrake', '@mail_gun', 
  //   '@objectrocket', '@rackspace', '@GeekdomSF', '@redistogo', 
  //   'mongodb', 'redis', 'elasticsearch', '#spark' ]}); 
  // stream = T.stream('statuses/filter', { locations: globe, track: ['black friday', 'blackfriday'], language: 'en'}); // filter tweets with geo data only
  // stream = T.stream('statuses/filter', { locations: globe, language: 'en'}); // filter tweets with geo data only
  // stream = T.stream('statuses/sample'); // filter tweets with geo data only
  // var stream = T.stream('statuses/filter', { track: topic }); // filter tweets with keyword
  stream.on('tweet', function (tweet) {
    // Create tweet object with geo data
    // if (tweet.coordinates || tweet.geo) {
      console.log(tweet.text);

      // keyword filter to extract and join keywords
      var extractedWords = extractor.extract(tweet.text, { language:"english", return_changed_case:true }).join(' ');

      var geo = tweet.coordinates == null ? [null, null] : tweet.coordinates.coordinates;
      if(geo[0]!=null) console.log("************************Geo!!!!!!**********************");

      // filter to check if offensive words are in tweet text
      var isBlacklisted = filter.blacklisted(tweet.text);

      var topicList=[];
      var wordsArray = cleanWords(extractedWords.split(' '));
      _topics.forEach(function(topic){
        var joined = _.intersection(topic.cleanKeywords, wordsArray);
        if(joined.length> 0) topicList.push(topic.topic);
      });
      console.log("Topics are: " + topicList);
      if(topicList.length==0) return; //skip tweets failed to get topic. But we are missing non english tweets
      var newTweet = {
        id: tweet.id,
        raw: tweet,
        created_at: tweet.created_at,
        photo: tweet.user.profile_image_url,
        description: tweet.text,
        text_keywords: extractedWords,
        followers_count: tweet.user.followers_count,
        in_reply_to_status_id: tweet.in_reply_to_status_id,
        in_reply_to_status_id_str: tweet.in_reply_to_status_id_str,
        in_reply_to_user_id: tweet.in_reply_to_user_id,
        in_reply_to_user_id_str: tweet.in_reply_to_user_id_str,
        in_reply_to_screen_name: tweet.in_reply_to_screen_name,
        screenName: tweet.user.screen_name,
        latitude: geo[1],
        longitude: geo[0],
        location: tweet.user.location,
        isBlacklisted: isBlacklisted,
        topic: topicList,
        keyword: "ebola"
      };
      if(tweet.retweeted_status!=undefined){
        console.log("==========================================found a retweet=====================================");
      }
      // console.log('t');

      // Save to database
      // console.log("valid tweet: " + newTweet.description);

      Tweet.create(newTweet);
    // }
  });
  callback(stream);
}

function initTopics(){
  var that = this;
  Topic.find({}, function(err, items){
    if(items.length == 0){
      console.log("no topic defined, seeding");
      seedTopics();
    }else{
      items.forEach(function(topic){
        topic.cleanKeywords=cleanWords(item.keywords),
       _topics.push(topic);
      })
      console.log("topics exist, skip seeding");
    }
  })

}

function cleanWords(arr){
  return arr.map(function(item){
    return item.toLowerCase().replace(/[^a-zA-Z_ ]/g, "");
  });
}


function resetTopics(){
  clearTopics();
  seedTopics();
}

function seedTopics(){
  var that = this;
  _topics.length = 0;
  defaultTopics.forEach(function(topic){
    Topic.create(topic);
    topic.cleanKeywords=cleanWords(topic.keywords),
    _topics.push(topic); 
  })
}

function clearTopics(){
  Topic.find({}, function(err, topics){
    for(var i=0; i<topics.length; i++){
      topics[i].remove();
    }
  });
}

function collectTopics(){
  console.log("==Collecting topics");
  Topic.find({}, function(err, topics){
    // console.log(topics);
    topics.forEach(function(item){
      Tweet.count({topic: item.topic}, function(err, cnt){
        var t = _topics.filter(function(t1){return t1.topic == item.topic;});
        if (t.length == 0){
          _topics.push({
            topic: item.topic, 
            keywords: item.keywords,
            cleanKeywords: cleanWords(item.keywords),
            count: cnt
          });
        }else{
          t[0].count = cnt;
        }
      })
    })
  })
  // console.log("collectTopics found: ");
  // console.log(_topics);
  return _topics;
}

module.exports = {
  streamTweets: streamTweets,
  stopTweets: stopTweets,
  initTopics: initTopics,
  resetTopics: resetTopics,
  seedTopics: seedTopics,
  clearTopics: clearTopics,
  collectTopics: collectTopics,
  streemOnTopics: streemOnTopics,
  topics: _topics
};

