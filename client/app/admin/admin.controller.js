'use strict';

angular.module('twitterGeyserApp')
  .controller('AdminCtrl', function ($scope, $http, Auth, User) {
    window.adminScope = $scope;
    $scope.streaming = false;
    $http.get('/api/tweets/firehoseStatus').success(function(status){
      console.log(status);
      $scope.streaming = status;
    });

    // starts twitter api stream
    $scope.startFirehose = function(){
      $http.post('/api/tweets/getTweets/ebola').success(function(){
        console.log('post success');
        $scope.streaming = true;
      });
    }

    // stops twitter stream
    $scope.stopFirehose = function(){
      $http.put('/api/tweets/getTweets/ebola').success(function(){
        console.log('stopped stream');
        $scope.streaming = false;
      });
    };

    $scope.resetTopics = function(){
      $http.get('/api/tweets/resetTopics').success(function(){
        console.log('resetted tracking topics');
      });
    };

    // Use the User $resource to fetch all users
    // $scope.users = User.query();

    // $scope.delete = function(user) {
    //   User.remove({ id: user._id });
    //   angular.forEach($scope.users, function(u, i) {
    //     if (u === user) {
    //       $scope.users.splice(i, 1);
    //     }
      // });
    // };
  });
