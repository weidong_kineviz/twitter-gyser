'use strict';

angular.module('twitterGeyserApp')

  .controller('MainCtrl', ['$scope', '$http', 'socket', 'Interceptor', function ($scope, $http, socket, Interceptor) {
    $scope.awesomeTweets = [];
    $scope.tweetParser = [];
    $scope.streaming = false;

    window.onkeydown = checkKeyPressed;
    window.ss = $scope;

    $scope.tweetTempStorage = {};
    $scope.topics = [];

    $scope.tweetsInContext = [{
      id:0,
      text:"tweet.text",
      created_by:"mock user",
      created_at:new Date(),
      count:1
    }];
    window.tweetsInContext = $scope.tweetsInContext;

    ///////// keydown event listener function...should probably go elsewhere //////////////
    
    function checkKeyPressed(e){
      console.log(e.keyCode);
      
      if (e.keyCode === 32) {
        e.preventDefault();
        drawing.vrControls().zeroSensor();
      }

      if (e.keyCode === 13) {
        e.preventDefault();
        $scope.getTweetsWithTopic($scope.topic);
      }

      if (e.keyCode === 81){
        e.preventDefault();
        $('#gui').toggle(1000);
      }
    }

    ////////// scope methods ///////////

    $scope.topics=[
    ];

    $scope.filterFunction=function(x){
      return x.count > 1 ? true : false;
    };

    $scope.initTopics=function(){
      $http.get('/api/tweets/getTopics').success(function(topics){
        topics.forEach(function(item){
          var t = $scope.topics.filter(function(t1){return t1.topic == item.topic;})
          if(t.length ==0){
            item.include = true;
            $scope.topics.push(item);
          }else{
            t[0].count = item.count;
          }
        })
      });
    }

    $scope.initTopics();
    setInterval($scope.initTopics, 10000);


    if(false){  //this should be retired, we don't pull all the tweets at the very beginning and process them anymore
      $http.get('/api/tweets').success(function(awesomeTweets) {
        $scope.awesomeTweets = awesomeTweets;
        // userGraph.processTweets(awesomeTweets);

        // creates object param for each topic in DB with total num of tweets
        for (var i = 0; i < awesomeTweets.length; i++){
          $scope.tweetTempStorage[awesomeTweets[i].keyword] = $scope.tweetTempStorage[awesomeTweets[i].keyword] || 0;
          $scope.tweetTempStorage[awesomeTweets[i].keyword]++;
        }

        // changes db summary object into array
        for (var key in $scope.tweetTempStorage) {
          $scope.newBucket = {};
          $scope.newBucket.topic = key;
          $scope.newBucket.numTweets = $scope.tweetTempStorage[key];

          if ($scope.tweetTempStorage[key] > 2000) {
            $scope.newBucket.isOptimal = "Yes";
          } else {
            $scope.newBucket.isOptimal = "No";
          }

          $scope.tweetParser.push($scope.newBucket);
        }
        // socket.syncUpdates('tweet', $scope.awesomeTweets);
      });
    }

    // **** add '/api/newtweets'

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('tweet');
    });

    // starts twitter api stream
    $scope.chooseTopic = function(){
      $scope.streaming = true;
      $http.post('/api/tweets/getTweets/ebola').success(function(){
        console.log('post success');
      });
    }

    // stops twitter stream
    $scope.stopTopic = function(){
      $scope.streaming = false;
      $http.put('/api/tweets/getTweets/ebola').success(function(){
        console.log('stopped stream');
      });
    };

    // fetches tweets matching topic from DB
    $scope.getTweetsWithTopic = function() {

      // Interceptor.start(); 
      // $('#gui').hide();
      var topicItems = _.reduce($scope.topics, function(sum, item){
        if(item.include){
          sum.push(item.topic);
        }
        return sum;
      }, []);

      if(topicItems.length == 0 )return;

      var startTime = userGraph.lastTweetTime;
      // $http.get('/api/tweets/getTweets/ebola/'+startTime).success(function(data){
      $http.get('/api/tweets/getTweets/'+ topicItems.toString()+'/'+startTime).success(function(data){

        // Interceptor.end();
        if(data.length>0){
          // renderTweets(data);
          userGraph.processTweets(data);
        }

      });
    };

    $scope.restart = function(){
      $scope.stop();
      userGraph.clear();
      userGraph.resetLastTweetTime();
      $scope.start();
    };

    $scope.start = function(){
      $scope.workerInterval = setInterval(function(){$scope.getTweetsWithTopic();}, 5000);  
    };

    $scope.stop = function(){
      clearInterval($scope.workerInterval);
    };
    
    $scope.start();

    // destroys all tweets matching topic in DB
    $scope.destroyTopic = function() {
      Interceptor.start();
      $http.delete('/api/tweets/getTweets/ebola').success(function(){
        Interceptor.end();
        console.log(topic, 'destroyed');
      })
    };

    $scope.startLayout=function(){
      userGraph.graph.layout.start_calculating();
    };

  }]);
