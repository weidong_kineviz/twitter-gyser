"use strict";

var twitterUserGraph=function(options){
	var maxNumUser = 150;
	var scene = options.scene;
	var graph = new Graph(options);
	var layoutOptions={};
	var lastTweetTime=new Date(Date.now() - 24*3600*1000); //seed date to 24 hours back

	// var tweetsInContext=[];
	// tweetsInContext = tweetsInContext;

	graph.layout = new Layout.ForceDirected(graph, layoutOptions);
	graph.layout.init();

	// options = {scene:drawing.scene};
	// var graph = new Graph(options);

	var processTweetsInOnce=function(tweets){
		console.log("batch processing tweets");
		tweets.forEach(function(tweet){
			processOneTweet(tweet.raw);
		})

	};
	
	var processTweets=function(tweets){
		var numTweets = tweets.length;
		lastTweetTime.setTime(new Date(tweets[numTweets-1].created_at).getTime());
		doProcessTweets(tweets);
	};
	
	var doProcessTweets=function(tweets){
		if(tweets.length<1) return

		var that = this;
		var tweet = tweets.shift();
		processOneTweet(tweet);
		graph.layout.init();
		graph.layout.resetTemperature();
		if(tweets.length>0)
			setTimeout(function(){doProcessTweets(tweets);}, 400); 
	};

	var removeOldestNode=function(){
		var node = graph.getOldestNode();
		node.data.tweets.forEach(function(item){
			var idx = tweetsInContext.indexOf(item);
			tweetsInContext.splice(idx,1);
		})
		graph.removeNode(node);
	};

	var processOneTweet=function(tweet){
		var node = createUserNode(tweet.raw.user);
		var date = new Date(tweet.raw.created_at);
		node.data.date = date;

		if(isRetweet(tweet.raw)){ //it's a retweet, we don't add text here, but add to orig node
			var orgNode = createUserNode(tweet.raw.retweeted_status.user);
			orgNode.data.date = new Date();
			orgNode.data.date.setTime(date.getTime());
			addTweetToUser(orgNode, tweet.raw.retweeted_status);
			var edge = graph.addEdge(orgNode, node);
			edge.draw();
		}else{
			addTweetToUser(node, tweet.raw);
		}
		renderTweet(tweet, node);

		if(graph.nodes.length > maxNumUser)
			removeOldestNode();

		if(graph.nodes.length > maxNumUser)
			removeOldestNode();
	};


	var userHasTweet=function(node, tweet){
		// if(node.data.tweets.length>3)
		// 	debugger;

		for(var i=0; i<node.data.tweets.length; i++){
			// if(node.data.tweets[i].text == tweet.text) 
			if(node.data.tweets[i].id == tweet.id) 
				return node.data.tweets[i];
		}
		return null;
	};

	var addTweetToUser=function(node, tweet){
		var twData = userHasTweet(node, tweet);
		if(twData!=null){ //found a match
			twData.count+=1;
		}else{
			var tweetData={
				id:tweet.id,
				text:tweet.text,
				created_by:node.data.userInfo.screen_name,
				created_at:tweet.created_at,
				count:1
			};

			node.data.tweets.push(tweetData);
			tweetsInContext.push(tweetData);
		}
		// tweetsInContext.sort(function(x,y){
		// 	return x.count< y.count;
		// });
	};


	var isRetweet=function(tweet){
		return tweet.retweeted_status != undefined;
	};

	// if user exist, return it, otherwise, create it and return
	var createUserNode=function(userInfo){
		// debugger;
		var node = graph.getNode(userInfo.screen_name);
		if(node == undefined){
			var node = new Node(userInfo.screen_name);
			node.data.userInfo=userInfo;
			node.data.tweets=[];
			node.position = new THREE.Vector3((Math.random()-0.5)*1000,
				(Math.random()-0.5)*1000,
				(Math.random()-0.5)*1000);
			graph.addNode(node);
			node.draw();
		}

		return node;		
	};

	var render=function(){
	    if(!graph.layout.finished) {
	      // info_text.calc = "<span style='color: red'>Calculating layout...</span>";
	      graph.layout.generate();
	      graph.edges.forEach(function(e){
			e.data.draw_object.geometry.verticesNeedUpdate = true;
	      });
	      
	      // updateEdgesSpline();
	    } else {
	      // info_text.calc = "";
	    }

	};

	var clear=function(){
		console.log("clearing graph to start new");
		graph.removeAllNodes();
		tweetsInContext.length = 0;
	};

	return {
		// graph:function(){return graph;},
		graph:graph,
		render:render,
		lastTweetTime:lastTweetTime,
		clear: clear,
		resetLastTweetTime:function(){lastTweetTime=new Date(Date.now() - 24*3600*1000);},
		processTweets:processTweets
		// tweetsInContext: tweetsInContext
	}
};