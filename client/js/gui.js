  "use strict"
//////////////////////// Dat GUI ////////////////////////////

 window.params = {
  followerThreshold:1000,
  wordThreshold:1,
  particleSize:10,
  particleFrequency:30,
  fountainHeight:2.3,
  timeMultiplier:1,
  cloudSpeed:0.0000003,

  // renderedDate:now,

  addEdge:true,
  showOffensive:false,
  showPhotos:false,
  
  rotationSpeed:0.1,

  toggleFullscreen:function() { drawing.toggleFullscreen(); }
};

var gui = new dat.GUI();

// set slider gui params
gui.domElement.id = 'gui';
gui.add(params, 'followerThreshold').min(100).max(10000).step(100);
gui.add(params, 'wordThreshold').min(1).max(100).step(1);
gui.add(params, 'particleSize').min(2).max(50).step(2);
gui.add(params, 'particleFrequency').min(10).max(50).step(5);
gui.add(params, 'fountainHeight').min(1).max(5).step(0.1);
gui.add(params, 'timeMultiplier').min(1).max(1000000).step(1);
gui.add(params, 'cloudSpeed').min(0.0000001).max(0.01).step(0.0000001)

// set filter gui params
gui.add(params, 'addEdge');
gui.add(params, 'showOffensive');
gui.add(params, 'showPhotos');

// set rotation gui params
gui.add(params, 'rotationSpeed').min(0).max(5).step(0.1)
  .onFinishChange(function(){
    orbitControls.autoRotateSpeed = params.rotationSpeed;
  });

// toggle fullscreen
gui.add(params, 'toggleFullscreen');
