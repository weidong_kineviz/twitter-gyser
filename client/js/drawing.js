"use strict";

/////////////////// Re-init on resize of window ///////////////////////
var bShowEarth = false;

var createDrawing=function(){
  var scene, camera, renderer, vrControls, objectSelection;
  var sun, clouds, globe, lensFlare;
  // debugger;
  var clock = new THREE.Clock();
  var bSelection = true;

  var onWindowResize=function() {
    var w, h;
    if (oculusIsOn){
      w = 1960;
      h = 1080;
      renderer.autoClear = false;
      oculusVr.setSize(w, h);
    } else if (isFullscreen) {
      w = window.innerWidth;
      h = window.innerHeight;
      renderer.autoClear = true;
      renderer.setSize(w, h);
    } else {
      var wOff = 0, hOff = 0;
      var el = renderer.domElement;
      
      while (el){
        wOff += el.offsetLeft;
        hOff += el.offsetTop;
        el = el.offsetParent;
      }
      
      w = window.innerWidth - wOff;
      h = window.innerHeight - hOff;
      renderer.autoClear = true;
      renderer.setSize(w, h);
    }
    camera.aspect = w/h;
    camera.updateProjectionMatrix();
  }

  ///////////// Removes sidebar and navbar to resize fullscreen window /////////////

  var toggleFullscreen = function () {
    if(isFullscreen){
      topic = $('.topic').val();
      $('.panel').show();
      $('.navbar').show();
      $('.footer').show();
      $('.display').addClass('ocOff');
      if(oculusIsOn) {
        $('#gui').show();
      }
      isFullscreen = false;
    }else{
      topic = $('.topic').val();
      $('.panel').hide();
      $('.navbar').hide();
      $('.footer').hide();
      $('.display').removeClass('ocOff');
      if(oculusIsOn) {
        $('#gui').hide();
      }
      isFullscreen = true;
    }
    //init();
    onWindowResize();
  };

  function enableSelection(){
    objectSelection = new THREE.ObjectSelection({
      domElement: renderer.domElement,
      selected: function(obj) {
        // display info
        if(obj != null) {
          // console.log(obj.org_node);
          // info_text.select = "Object " + obj.org_node.data.email;
        } else {
          // delete info_text.select;
        }
      },
      clicked: function(obj) {
        var txt = obj.org_node.data.userInfo.screen_name;
        obj.org_node.data.tweets.forEach(function(tw){
          txt = txt + "\n"+tw.text;
        })
        alert(txt);
        // var email_msg = obj.org_node == undefined ? "empty" : obj.org_node.data.email;
        // var msg = "Node,"+obj.org_node.id+","+email_msg+",,0";
        // alert(msg);
        // console.log("Object real id:" + obj.org_node.id+", email: "+email_msg);
      }
    });
  }

  function turnOnSelection(){
    selection = true;
    if(object_selection==undefined)
      enableSelection();
  }
  function turnOffSelection(){
    selection = false;
  }


  ///////////// THREE.js globe visualization and VR ////////////////

  var render = function () {
    if(userGraph!=undefined) userGraph.render();
    if(oculusIsOn) {
      oculusVr.render(scene, camera);
    } else {
      renderer.autoClear = true;
      renderer.render(scene, camera);
    }

    if(bSelection) {
      objectSelection.render(scene, camera);
    }
  };

  var now = new Date();

  var updateSunAndEarth = function (delta) {
    var k = params.timeMultiplier;
    now.setUTCMilliseconds(now.getUTCMilliseconds() + Math.floor(delta * 1000) * k);

    clouds.rotation.y += params.cloudSpeed * k;
    globe.rotation.y = THREE.Math.degToRad(globe.siderealTime(now));
    var solar = globe.solarCoordinates(now);
    var vec = globe.geoToEcef(solar.δ, -solar.α, solar.R * 23454.791 * earth_radius);
    sun.position.copy(vec);
    lensFlare.position.copy(vec);
  };

  var animate = function () {
    var dt = clock.getDelta();
    orbitControls.update(dt);
    if(oculusIsOn && oculusControl) {
      vrControls.update();
    }
    if(bShowEarth)
      updateSunAndEarth(dt);

    scene.traverse(function (mesh) {
      if(mesh.update !== undefined) {
        mesh.update();
      }
    });

    requestAnimationFrame(animate);
    TWEEN.update();
    render();
  };

  //////////////// Initializes globe at appropriate settings based on oculus flag ///////////////////////

  var init = function () {
    //$('#container').empty();
    var container = document.getElementById('webgl-container');
    var wInit = 0, hInit = 0;
    var el = container;

    while (el) {
      wInit += el.offsetLeft;
      hInit += el.offsetTop;
      el = el.offsetParent;
    }

    // overrides w & h if oculus headset is flagged on
    if(oculusIsOn) {
      wInit = 0;
      hInit = 0;
    }

    var w = window.innerWidth - wInit;
    var h = window.innerHeight - hInit;

    scene = new THREE.Scene();
    if(bShowEarth)
      globe = new Globe(earth_radius, scene);

    camera = new THREE.PerspectiveCamera(65, w / h, 5, 50000 * earth_radius);
    vrControls = new THREE.VRControls(camera);
    vrControls.enablePosition = false;

    renderer = new THREE.WebGLRenderer({
      precision   : 'highp',
      //preserveDrawingBuffer: true,
      antialiasing: true,
      alpha       : true
    });

    renderer.clearColor = new THREE.Color(0x000000, 1.0);
    renderer.clear();

    renderer.gammaInput = true;
    renderer.gammaOutput = true;

    scene.add(new THREE.AmbientLight(0x404040));

    sun = new THREE.DirectionalLight(0xffffff, 1.5);
    sun.position.set(0, 0, 0);
    scene.add(sun);

    if(bShowEarth){
      var textureFlare0 = THREE.ImageUtils.loadTexture("../assets/images/lensflare0.png");
      var textureFlare2 = THREE.ImageUtils.loadTexture("../assets/images/lensflare2.png");
      var textureFlare3 = THREE.ImageUtils.loadTexture("../assets/images/lensflare3.png");
      lensFlare = new THREE.LensFlare(textureFlare0, 700, 0, THREE.AdditiveBlending, new THREE.Color(0xffffff));
      lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
      lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
      lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
      lensFlare.add(textureFlare3, 60, 0.6, THREE.AdditiveBlending);
      lensFlare.add(textureFlare3, 70, 0.7, THREE.AdditiveBlending);
      lensFlare.add(textureFlare3, 120, 0.9, THREE.AdditiveBlending);
      lensFlare.add(textureFlare3, 70, 1.0, THREE.AdditiveBlending);
      lensFlare.customUpdateCallback = function (obj) {
        var f, fl = obj.lensFlares.length;
        var flare;
        var vecx = -obj.positionScreen.x * 2;
        var vecy = -obj.positionScreen.y * 2;
        for(f = 0; f < fl; f++) {
          flare = obj.lensFlares[f];
          flare.x = obj.positionScreen.x + vecx * flare.distance;
          flare.y = obj.positionScreen.y + vecy * flare.distance;
          flare.rotation = 0;
        }
        obj.lensFlares[2].y += 0.025;
        obj.lensFlares[3].rotation = obj.positionScreen.x * 0.5 + THREE.Math.degToRad(45);
      };
      lensFlare.position.copy(sun.position);
      scene.add(lensFlare);
    }

    camera.lookAt(scene.position);
    camera.position.set(0.0, 0.0, 1200);
    renderer.setSize(w, h);
    if(bShowEarth)
      globe.position.set(0.0, 0.0, 0.0);

    var skybox = new THREE.Mesh(new THREE.SphereGeometry(25000 * earth_radius, 72, 36), new THREE.MeshBasicMaterial({
      map : THREE.ImageUtils.loadTexture('../assets/images/tycho3.png'),
      side: THREE.BackSide
    }));

    //clouds object
    if(bShowEarth){
      var cloud_scale = 1.02;
      var cloudsGeometry = new THREE.SphereGeometry(earth_radius * cloud_scale, 40, 40);
      var cloudsTexture = THREE.ImageUtils.loadTexture('../assets/images/clouds.png');
      cloudsTexture.anisotropy = renderer.getMaxAnisotropy();
      cloudsTexture.wrapS = THREE.RepeatWrapping;
      var cloudsMaterial = new THREE.MeshLambertMaterial({
        map          : cloudsTexture,
        transparent  : true,
        blending     : THREE.CustomBlending,
        blendSrc     : THREE.SrcAlphaFactor,
        blendDst     : THREE.OneMinusSrcColorFactor,
        blendEquation: THREE.AddEquation
      });

      clouds = new THREE.Mesh(cloudsGeometry, cloudsMaterial);

      // create scene
      globe.add(clouds);
      scene.add(globe);
    }
    scene.add(skybox);
    container.appendChild(renderer.domElement);

    window.oculusVr = new THREE.OculusRiftEffect(renderer);
    oculusVr.setSize(w, h);

    // sets up orbitcontrols and limits mouse inputs to container element
    window.orbitControls = new THREE.OrbitControls(camera, renderer.domElement);
    orbitControls.minPolarAngle = Math.PI / 4;
    orbitControls.maxPolarAngle = 3 * Math.PI / 4;
    orbitControls.minDistance = 250;
    orbitControls.maxDistance = 2000;
    orbitControls.autoRotate = true;
    orbitControls.autoRotateSpeed = params.rotationSpeed;

    if(bSelection)
      enableSelection();
    animate();
  };
  return{
    vrControls:function(){return vrControls;},
    // globe:function(){return globe;},
    camera:function(){return camera;},
    scene:function(){return scene;},
    onWindowResize: onWindowResize,
    toggleFullscreen: toggleFullscreen,
    init: init
  }
};